# Nodejs project

### Lors de l'installation, il faut tout d'abord lancer la commande :
```
npm install
```
Cela permet de créer le dossier node_modules

### Ensuite, pour lancer l'application, il faut lancer la commande :
```
node app.js
```
Le serveur tourne maintenant sur le port 4000

### Pour utiliser l'application, il suffit maintenant d'ouvrir un navigateur et d'aller à l'url :
```
localhost:4000
```

### Les fonctionnalités sont les suivantes :
* Ajouter un film, une série ou une musique
* Les modifier
* Les supprimer
* Consulter la page information qui est différente selon si c'est un film, une musique ou une série. On peut également changer les informations de cette page
* Dans le tableau principal on peut :
  * Trier par type croissant
  * Trier par type décroissant
  * Trier par nom croissant
  * Trier par nom décroissant
  * Exporter les informations au format csv (dans un fichier nommé pref.csv)
  * Exporter les informations au format json (dans un fichier nommé pref.json)
